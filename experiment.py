import os
import sys
import ssh_lib as ssh
import time

# Client (local)
# Server (remote)

EXPERIMENT_DURATION_SECONDS = 60
PRODUCER_CONSUMER_OFFSET_SECONDS = 20

SERVER_IP = "192.168.8.102"
SERVER_USER = "nfd"
SERVER_PWD = "nfd"
SERVER_REMOTEDIR = "/home/nfd/push_pull_experiment/"


CLIENT_CMDFILE = "client.sh"
SERVER_CMDFILE = "server.sh"
MEASURE_CMDFILE = "measure.sh"
MEASURE_SCRIPT = "log.py"

def get_setup_cmds():
    return ["#!/bin/bash", "sudo nfd-stop", "sleep 5", "sudo nfd-start > /dev/null", "sleep 5"]

def get_pull_client_cmdlist(num_streams):
    # ./pull-consumer --own-id 2 -p /voice/2 -r 100
    cmds = ["#!/bin/bash"]
    cmds.append("timeout " + str(EXPERIMENT_DURATION_SECONDS) + "s parallel -j0 --plus pull-consumer --own-id {#} --prefix /voice/{#} --rate 100 ::: {1.." + str(num_streams) + "}")
    cmds.append("\n")
    return cmds

def get_push_client_cmdlist(num_streams):
    # ./push-consumer --own-id 1 --prefix /voice/1 --refresh-interval 1000 --lifetime 4000
    cmds = ["#!/bin/bash"]
    cmds.append("timeout " + str(EXPERIMENT_DURATION_SECONDS) + "s parallel -j0 --plus push-consumer --own-id {#} --prefix /voice/{#} --refresh-interval 1000 --lifetime 4000 ::: {1.." + str(num_streams) + "}")
    cmds.append("\n")
    return cmds

def get_measure_cmdlist():
    cmds = ["#!/bin/bash"]
    cmds.append("cd " + SERVER_REMOTEDIR)
    cmds.append("python " + MEASURE_SCRIPT + " nfd " + str(EXPERIMENT_DURATION_SECONDS))
    cmds.append("\n")
    return cmds

def get_pull_server_cmdlist(num_streams):
    # ./pull-producer --own-id 1 --prefix /voice/1 --data-size 80
    cmds = ["#!/bin/bash"]
    cmds.append("cd " + SERVER_REMOTEDIR)
    cmds.append("timeout " + str(EXPERIMENT_DURATION_SECONDS+PRODUCER_CONSUMER_OFFSET_SECONDS) + "s parallel -j0 --plus pull-producer --own-id {#} --prefix /voice/{#} --data-size 80 ::: {1.." + str(num_streams) + "}")
    cmds.append("\n")
    return cmds

def get_push_server_cmdlist(num_streams):
    # ./push-producer --own-id 1 --prefix /voice/1 --data-size 80 --rate 100
    cmds = ["#!/bin/bash"]
    cmds.append("cd " + SERVER_REMOTEDIR)
    cmds.append("timeout " + str(EXPERIMENT_DURATION_SECONDS+PRODUCER_CONSUMER_OFFSET_SECONDS) + "s parallel -j0 --plus push-producer --own-id {#} --prefix /voice/{#} --data-size 80 --rate 100 ::: {1.." + str(num_streams) + "}")
    cmds.append("\n")
    return cmds


def measurement_run(variant, num_streams, parent_result_dir):
    print "beginning execution of " + variant + "-run"
    # establish ssh-connections
    server_ssh = ssh.Connection(SERVER_IP, username=SERVER_USER, password=SERVER_PWD)

    result_dir = os.path.join(parent_result_dir, variant)
    os.mkdir(result_dir)


    # setup
    setup_cmds = get_setup_cmds()
    with open(CLIENT_CMDFILE, "w") as client_cmdfile:
        client_cmdfile.write("\n".join(setup_cmds + ["sleep 5", "sudo nfdc register /voice udp4://" + SERVER_IP]) + "\n")

    with open(SERVER_CMDFILE, "w") as server_cmdfile:
        server_cmdfile.write("\n".join(setup_cmds)  + "\n")

    # push setup-scripts to nodes and execute them
    print "starting remote setup (server)"
    remote_file = SERVER_REMOTEDIR + SERVER_CMDFILE
    print server_ssh.put(SERVER_CMDFILE, remote_file)
    print server_ssh.execute("chmod +x " + remote_file)
    print server_ssh.execute("screen -d -m " + remote_file)

    print "starting local setup (client)"
    print os.system("chmod +x " + CLIENT_CMDFILE)
    print os.system("./" + CLIENT_CMDFILE)

    print "\n\nwaiting for 10s in order to complete nfd-restart on nodes"
    time.sleep(10)

    #raw_input("(All NFDs restarted?) Press Enter to continue...")

    # create runtime-scripts to start the experiment on all nodes
    with open(CLIENT_CMDFILE, "w") as client_cmdfile:
        if variant == "PULL":
            client_cmdfile.write("\n".join(get_pull_client_cmdlist(num_streams)))
        else:  # PUSH
            client_cmdfile.write("\n".join(get_push_client_cmdlist(num_streams)))

    with open(MEASURE_CMDFILE, "w") as measure_cmdfile:
        measure_cmdfile.write("\n".join(get_measure_cmdlist()))

    with open(SERVER_CMDFILE, "w") as server_cmdfile:
        if variant == "PULL":
            server_cmdfile.write("\n".join(get_pull_server_cmdlist(num_streams)))
        else: # PUSH
            server_cmdfile.write("\n".join(get_push_server_cmdlist(num_streams)))

    # deploy runtime-scripts
    print "deploying runtime-scripts"

    remote_servercmd_file = SERVER_REMOTEDIR + SERVER_CMDFILE
    print server_ssh.put(SERVER_CMDFILE, remote_servercmd_file)
    print server_ssh.execute("chmod +x " + remote_servercmd_file)

    print server_ssh.put(MEASURE_SCRIPT, SERVER_REMOTEDIR + MEASURE_SCRIPT) # re-deploy logging script

    remote_measurecmd_file = SERVER_REMOTEDIR + MEASURE_CMDFILE
    print server_ssh.put(MEASURE_CMDFILE, remote_measurecmd_file)
    print server_ssh.execute("chmod +x " + remote_measurecmd_file)

    # execute runtime-scripts
    print "starting runtime-scripts"
    print server_ssh.execute("screen -d -m " + remote_servercmd_file)  # producer-apps
    time.sleep(PRODUCER_CONSUMER_OFFSET_SECONDS)
    print server_ssh.execute("screen -d -m " + remote_measurecmd_file) # measurement-script (start logging script)
    os.system("./" + CLIENT_CMDFILE)                                   # consumer-apps, blocks until finished


    # wait for experiment-time to elapse
    time.sleep(PRODUCER_CONSUMER_OFFSET_SECONDS)
    print "experiment should be finished now"

    # collect results from client (local)
    print "collecting local results (client)"
    os.system("mv *.log " + result_dir)

    # collect results from server (remote)
    print "collecting remote results (server)"
    dest_dir = os.path.abspath(result_dir)
    listing = server_ssh.execute("ls " + SERVER_REMOTEDIR + " | grep .log")
    for file in listing:
        filename = file.rstrip()
        print "copying " + SERVER_REMOTEDIR + filename + " to " + result_dir + "/" + filename
        server_ssh.get(SERVER_REMOTEDIR + filename, os.path.join(dest_dir,filename))

    print "run completed\n\n\n"
    server_ssh.close()


# main entry-point of the program
# for a given name and number of concurrent streams, a push and pull-run is executed

if len(sys.argv) < 3:
    exit("usage: " + sys.argv[0] + " name " + "number_of_concurrent_streams")

name = sys.argv[1]
num_streams = int(sys.argv[2])

# create result-directory
result_dir = name + "_" + str(num_streams)
os.mkdir(result_dir)

measurement_run("PULL", num_streams, result_dir)
measurement_run("PUSH", num_streams, result_dir)