import psutil as ps
import os
import threading
import subprocess
import glob
from time import sleep

OUTPUTFILENAME = "c10k.log"
OUTPUTDIRECTORY = OUTPUTFILENAME.split('.')[0]

class Thread(threading.Thread):
    # init
  def __init__(self, sys_cal):
		super(Thread,self).__init__()
		self.sysCall = sys_cal

  # overwriting run method of threading.Thread (do not call this method, call thread.start() )
  def run(self):

		global LOGGING

		fpOut = open(OUTPUTDIRECTORY + "/app.stdout.txt", "w")

		print self.sysCall

		# start subprocess
		proc = subprocess.Popen(self.sysCall,stdout=fpOut,cwd=os.getcwd())
		proc.communicate() # wait until finished
		fpOut.close()

		print "Applicaiton Finished"
		LOGGING = False

print "Start logging with version: " +ps.__version__

os.mkdir(OUTPUTDIRECTORY,0755)


resultFile = open(OUTPUTDIRECTORY + "/" + OUTPUTFILENAME, "w");
resultFile.write("nI \t AvgCPU-load \t Satisfaction-ratio\n")

for rate in xrange(100,900,100):
	LOGGING = True

	print str(rate) + " interests/second: start"	
	SYSCALL = ["consumer"] + ["--prefix=/test" + str(rate)] + ["--rate=" + str(rate)] + ["--lifetime=1000"] + ["--logfile=" + OUTPUTDIRECTORY + "/" + str(rate) + "_consumer.log"] + ["--run-time=60"]

	thread = Thread(SYSCALL)
	thread.start()

	PROCNAME = "nfd"
	pid = 0
	p = None
	cpu_counter = 0.0

	log = open(OUTPUTDIRECTORY + "/" + str(rate) +"_time.log","w")
	log.write("syscall=" + str(SYSCALL) + "\n")
	log.write("id, cpu_percent\n")

	for proc in ps.process_iter():
		if proc.name() == PROCNAME:
			p = proc
			break

	if p == None:
		print "Could not find specified Process! Exiting..."
		exit(-1)

	counter = 0
	
	while LOGGING:
		percent = p.cpu_percent(interval=1.0)
		log.write(str(counter)+", "+str(percent)+"\n")
		cpu_counter += percent
		counter+=1

	# get sat-rati
	line = subprocess.check_output(['tail', '-1', OUTPUTDIRECTORY + "/" + str(rate) + "_consumer.log"])
	line = line.replace("\n","")
	satRatio = line.split(':')[1]
	
	log.write("AVG CPU-Usage=" + str(cpu_counter/counter))
	log.close()
	resultFile.write(str(rate) + "\t" + str(cpu_counter/counter) + "\t" + satRatio +"\n")
	print str(rate) + " interests/second: stop, logging results\n\n" 

resultFile.close();
print "finished"
exit(0)
	
		
		
