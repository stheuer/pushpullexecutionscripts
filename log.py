import psutil as ps
import sys
import datetime

# main entry point
if len(sys.argv) < 3:
	exit("usage: " + sys.argv[0] + " <process-name> <record-duration in seconds>")

PROCNAME = sys.argv[1]
duration = datetime.timedelta(seconds=int(sys.argv[2]))
pid = 0
p = None
cpu_counter = 0.0

print "Start logging with psutils-version: " + ps.__version__
print "logging for " + str(duration) + ", target-file: ./" + PROCNAME + ".log"


log = open("./" + PROCNAME + ".log", "w")
log.write("id, cpu_percent, mem_percent\n")

for proc in ps.process_iter():
	if proc.name() == PROCNAME:
		p = proc
		break

if p == None:
	print "Could not find specified Process! Exiting..."
	exit(-1)

counter = 0
start_time = datetime.datetime.now()
end_time = start_time + duration

while datetime.datetime.now() <= end_time:
    percent = p.cpu_percent(interval=1.0)
    log.write(str(counter) + ", " + str(percent) + ", " + str(p.memory_percent()) + "\n")
    cpu_counter += percent
    counter += 1

log.close()
print "finished"
exit(0)