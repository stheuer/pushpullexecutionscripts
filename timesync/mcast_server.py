# based on https://pymotw.com/2/socket/multicast.html (big thanks!)
import sys
import socket
import struct
import datetime

BUFFER_SIZE = 4096
MULTICAST_GROUP = '224.3.29.71'
SERVER_ADDRESS = ('', 10000)

STATUSFILE_PATH = "/var/run/ptpd2.status"

"""
Remotely callable methods
"""
def ptp_offset():
    access_time = datetime.datetime.now()
    with open(STATUSFILE_PATH, "r") as status_file:
        lines = status_file.readlines()
        if len(lines) < 17:  # insufficient lines
            return ""
        offset = lines[16].split()
        if len(offset) >= 5:
            return '{}|{}'.format(access_time, offset[4])
        else:
            return "" # not enough columns in line



"""
UDP-Multicast stuff
"""
# Create the socket
sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

# Bind to the server address
sock.bind(SERVER_ADDRESS)

# Tell the operating system to add the socket to the multicast group
# on all interfaces.
group = socket.inet_aton(MULTICAST_GROUP)
mreq = struct.pack('4sL', group, socket.INADDR_ANY)
sock.setsockopt(socket.IPPROTO_IP, socket.IP_ADD_MEMBERSHIP, mreq)
print >>sys.stderr, "mcast", MULTICAST_GROUP, SERVER_ADDRESS[1], 'ready'

# Receive/respond loop
while True:
    request, address = sock.recvfrom(BUFFER_SIZE) # request contains the function to be called
    try:
        response = locals()[request]() # dispatch remote procedure call
    except Exception as e:
        sock.sendto(str(e),address)
    else:
        sock.sendto(response, address)