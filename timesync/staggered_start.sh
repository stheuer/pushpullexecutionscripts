#!/bin/bash


# the purpose of this script is to speed up the convergence
# by repeatedly killing/restarting the ptp-deamon in order
# to overcome premature "settling down" / reduction of "Clock correction"
# "find out of a perceived local optimum
# Extras: enabling the performance governor and granting ptpd higher process-priority

# not strictly necessary
#sudo sh gov.sh performance

for i in 1 2 3 4
do
echo "\nstep" $i
sudo killall ptpd2
#sudo chrt -r 50 /usr/local/sbin/ptpd2 -c /etc/ptpd.conf
sudo /usr/local/sbin/ptpd2 -c /etc/ptpd.conf
sleep 180
done
echo "\ndone"

