# pushPullForwardingPerformanceComparisonScripts


## Network-Setup
```
phys network: _________________________
client <==>  |                        |
             |Gigabit Ethernet-Switch | <==> router (BPR1)
server <==>  |________________________| 

logical network (overlay achieved by NFD-routing [nfdc]):
client <==> router <==> server


Example-IPs:
client ... 192.168.0.2
router ... 192.168.0.11
server ... 192.168.0.3
```

## Node-Setup
client and server are COTS hardware running Ubuntu 16.04.3 LTS

router is a BananaPi R1 (BPR1, Lamobo R1) Router running a custom version of Bananian 16.04 obtainable from http://icn.itec.aau.at/

All nodes have (for simplicity's sake) the ndn-cxx, NFD, applications and execution-scripts installed (obtainable from https://github.com/phylib/PersistentInterest).


## Execution-Preparation
3node_experiment.py is the main script controlling almost all aspects of the execution of the comparison.

* Ensure that the IP-addresses and SSH-credentials mentioned in 3node_experiment.py match with reality.
* Execute execution-scripts/gov.sh to use the performance-CPU-scheduler on the router-node (sudo sh gov.sh performance)
* (Optional) Ensure time-synchronization between client and server (if needed; execution-scripts/timesync/staggeredstart.sh can be used [PTP, requires ptpd [at least 2.3.1] be installed on respective nodes])

## Execution
* On the client-node: Execute 3node_experiment.py by calling ```python 3node_experiment.py pushpull 70``` 

pushpull is the folder-prefix for the results, 70 represents the number of concurrent voice-streams to emulate.
One can automate the whole procedure by chaining these commands using a bash-script as exemplified by experiment.sh.

A execution-run encompasses:

1. the stop and re-start of all NFD instances (one on every node)

2. the registration of routes for the NFDs on the client and router

3. the start of all producer-apps on the server

4. the start of all consumer-apps on the client

5. the collection of all relevant results / logfiles once the execution-time is over

All of the above is performed once with pull-based producer-/consumer-pairs and once with push-based producer-/consumer-pairs

The results (server- and client-logs as well as the CPU/mem-log of the NFD-process on the router) are collected in the current directory organized in own subdirectories e.g. pushpull_70/PULL and pushpull_70/PUSH.
Those results are the raw-material for the comparison/evaluation achieved with the Evaluation-scripts (obtainable from https://github.com/phylib/PersistentInterest/tree/master/PerformanceEvaluation).


## Misc Infos
* experiment.py is a two-node version of the normal (three-node) experiment.
* log.py creates a CPU/memory-log for a given process-name (used to log NFD-"status") 
* gov.sh is used to set the CPU-governor of a node (here only ondemand and performance)
* the content of the folder timesync is only relevant if PTP-timesync using ptpd is used (means of starting ptpd and visualization of timesync-quality)
* in our experiments, the client acted as PTP-Master and the server and router as PTP-slaves (we tried to measure the runtime/latency of packets too)

```
bitvector-size: 200
refresh-interval: 2s
interest-lifetime: 5s
100 packets per second a 80byte payload
cpu-scheduler ondemand on client and server
cpu-scheduler performance (throughout max. CPU-freq) on router

nfd-CS (contentstore) is disabled
   http://www.lists.cs.ucla.edu/pipermail/nfd-dev/2017-February/002257.html
   Apparently, it is enough to set "cs_max_packets 0" in the NFD-config-file  /usr/local/etc/ndn/nfd.conf

```

